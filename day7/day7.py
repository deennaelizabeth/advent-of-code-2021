from abc import abstractclassmethod
import statistics
import math

with open('./day7/input7.txt') as f:
    lines = f.readlines()
    sealTeamCrab = lines[0].strip().split(',')
    crabs = list(map(int, sealTeamCrab))
    crabsAssemble = math.floor(statistics.median(crabs))
    fuel = 0
    for crab in crabs:
        fuel += abs(crab - crabsAssemble)
    print(f'Part One: {fuel}')

with open('./day7/input7.txt') as f:
    lines = f.readlines()
    sealTeamCrab = lines[0].strip().split(',')
    crabs = list(map(int, sealTeamCrab))
    lowAverageCrabsAssemble = math.floor(statistics.mean(crabs))
    highAverageCrabsAssemble = math.ceil(statistics.mean(crabs))
    lowFuel = 0
    highFuel = 0
    for crab in crabs:
        lowDistance = abs(crab - lowAverageCrabsAssemble)
        highDistance = abs(crab - highAverageCrabsAssemble)
        lowFuel += (lowDistance * (lowDistance + 1)) / 2 
        highFuel += (highDistance * (highDistance + 1)) / 2
    print(f'Part One: {fuel}')   
    print(f'Part Two: {min(lowFuel, highFuel)}')

    #END OF PROGRAM