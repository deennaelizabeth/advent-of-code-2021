import math
points = []
folds = []
maxes = {'xMax': 0, 'yMax': 0}
#function to determine where the fold is and places #s in new grid
def fold(grid, axis, foldVal):
    returnGrid = []
    if axis == 'x':
        for y in range(maxes['yMax'] + 1):
            gridTwoX = maxes['xMax']
            line = []
            for x in range(foldVal):
                if grid[y][x] == '#' or grid[y][gridTwoX] == '#':
                    line.append('#')
                else:
                    line.append('.')
                gridTwoX -= 1
            returnGrid.append(line)
        maxes['xMax'] = foldVal - 1
    elif axis == 'y':
        gridTwoY = maxes['yMax']
        for y in range(foldVal):
            line = []
            for x in range(maxes['xMax'] + 1):
                if grid[y][x] == '#' or grid[gridTwoY][x] == '#':
                    line.append('#')
                else:
                    line.append('.')
            gridTwoY -= 1
            returnGrid.append(line)
        maxes['yMax'] = foldVal - 1
    return returnGrid
#end of function
with open('./day13/input13.txt') as f:
    lines = f.readlines()
    for line in lines:
        coordinate = line.strip().split(',')
        numCoordinate = list(map(int, coordinate))
        points.append((numCoordinate[0], numCoordinate[1]))
        maxes['xMax'] = max(numCoordinate[0], maxes['xMax'])
        maxes['yMax'] = max(numCoordinate[1], maxes['yMax'])
with open('./day13/folds.txt') as f:
    lines = f.readlines()
    for line in lines:
        instruction = line.strip().split('=')
        numInstruction = int(instruction[1])
        axis = instruction[0]
        folds.append((axis, numInstruction))
#print(maxes['xMax'], maxes['yMax'], folds) check for parsing
grid = []
for y in range(maxes['yMax'] + 1):
    line = []
    for x in range(maxes['xMax'] + 1):
        if (x, y) in points:
            line.append('#')
        else:
            line.append('.')
    grid.append(line)
# (axis, value) = folds[0]
# grid = fold(grid, axis, value)
# count = 0
# for line in grid:
#     print(line)
#     for char in line:
#         if char == '#' : count += 1
# print(count)

#end of part one

for (axis, value) in folds:
    print('beep boop thinking :)')
    grid = fold(grid, axis, value)
for line in grid:
    print(''.join(line))