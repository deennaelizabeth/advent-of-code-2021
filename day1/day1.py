import csv

file = open('./day1/input1.csv')

csvreader = csv.reader(file)

next (csvreader)
count = 0
previousValue = 0
input  = []
for row in csvreader:
    value = int(row[0])
    input.append(value)
    if previousValue == 0:
        #print('%s No Previous Value') this checks the if statement
        pass
    elif previousValue > value:
        #print('%s decreased' % value) this checks the elif statement
        pass
    else:
        #print('%s increased' % value) this checks the else statement
        count += 1
    previousValue = value

print('Part One')
print(count)

#end of part one

count = 0
previousSum = 0
i = 0
while i < len(input) - 2:
    a = input[i]
    #print(a) this checks variable 'a'
    b = input[i + 1]
    #print(b) this checks variable 'b'
    c = input[i + 2]
    #print(c) this checks variable 'c'
    sum = a + b + c
    if previousSum == 0:
        print('%s No Previous Value') #this checks the if statement
        pass
    elif previousSum >= sum:
        print('%s decreased' % sum) #this checks the elif statement
        pass
    else:
        print('%s increased' % sum) #this checks the else statement
        count += 1
    previousSum = sum
    i = i + 1

print('Part Two')
print(count)

#end of part two