from os import X_OK


with open('./day6/input6.txt') as f:
    lines = f.readlines()
    days = 0
    initialString = lines[0]
    fishString = initialString.split(',')
    fishes = list(map(int, fishString))
    print(fishes)
    for day in range(80):
        currentDay = fishes.copy()
        for i, f in enumerate(currentDay):
            if f != 0 : fishes[i] -= 1
            elif f == 0 :
                fishes[i] = 6
                fishes.append(8)
    print('Part One Answer')
    print (len(fishes))    

#end of part one

with open('./day6/input6.txt') as f:
    lines = f.readlines()
    fishList = [0, 0, 0, 0, 0, 0, 0, 0, 0,]
    initialString = lines[0]
    fishString = initialString.split(',')
    fishes = list(map(int, fishString))
    for fish in fishes:
        fishList[fish] += 1
    for day in range(256):
        zeros = fishList[0]
        for i in range(len(fishList) - 1):
            fishList[i] = fishList[i + 1]
        fishList[6] += zeros
        fishList[8] = zeros
    sum = 0
    for totals in fishList:
        sum += totals
    print('Part Two Answer')
    print(sum)
    