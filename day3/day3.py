import math
with open('./day3/input3.txt') as f:
    lines = f.readlines()
    bitList = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    gamma = ''
    epsilon = ''
    for x in range(12):
        for line in lines:
            bit = line.strip()[x]
            if bit == '1' : bitList[x] += 1
    
    for x in bitList:
        if x >= 500 : 
            gamma += '1'
            epsilon += '0'
        else:
            gamma += '0'
            epsilon += '1'
    print(gamma, epsilon)

gammaParsed = int(gamma, 2)
epsilonParsed = int(epsilon, 2)
print(gammaParsed * epsilonParsed)

#end of part one

with open('./day3/input3.txt') as f:
    lines = f.readlines()
    filteredList = [lines, [], [], [], [], [], [], [], [], [], [], [], []]
    for x in range(12):
        commonOutput = 0
        for line in filteredList[x]:
            bit = line.strip()[x]
            if bit == '1' : commonOutput += 1
        moreCommon = '0'
        if commonOutput >= len(filteredList[x]) / 2 : moreCommon = '1'
        for line in filteredList[x]:
            if line.strip()[x] == moreCommon:
                filteredList[x+1].append(line)

    O2Rate = int(filteredList[-1][0].strip(),2)

with open('./day3/input3.txt') as f:
    lines = f.readlines()
    filteredList = [lines, [], [], [], [], [], [], [], [], [], [], [], []]
    for x in range(12):
        commonOutput = 0
        for line in filteredList[x]:
            bit = line.strip()[x]
            if bit == '1' : commonOutput += 1
        lessCommon = '0'
        if commonOutput < (len(filteredList[x]) / 2) : lessCommon = '1'
        for line in filteredList[x]:
            if line.strip()[x] == lessCommon or len(filteredList[x]) == 1:
                filteredList[x+1].append(line)
  
    CO2Rate = int(filteredList[-1][0].strip(),2)

    print('SURVEY SAYS')
    print(O2Rate * CO2Rate)

    #WE MADE IT YA'LL