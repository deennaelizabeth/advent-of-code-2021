with open('./day2/input2.txt') as f:
    lines = f.readlines()
    horizontalPosition = 0
    depth = 0
    for line in lines:
        direction = line.strip().split(' ')
        print(direction)
        command = direction[0]
        number = int(direction[1])
        match command:
            case 'forward':
                horizontalPosition += number
            case 'down':
                depth += number
            case 'up':
                depth -= number
            case _:
                print('not found')
    print(horizontalPosition * depth)

    # end of part one

with open('./day2/input.txt') as f:
    lines = f.readlines()
    horizontalPosition = 0
    depth = 0
    aim = 0
    for line in lines:
        direction = line.strip().split(' ')
        print(direction)
        command = direction[0]
        number = int(direction[1])
        match command:
            case 'forward':
                horizontalPosition += number
                depth += aim * number
            case 'down':
                aim += number
            case 'up':
                aim -= number
            case _:
                print('not found')
    print(horizontalPosition * depth)

#end of program