import statistics
import math
OPENERS = ['[', '{', '(', '<']
CLOSERS = [']', '}', ')', '>']
scoreLookUp = {
    ')' : 3,
    ']' : 57,
    '}' : 1197,
    '>' : 25137,
    '(' : 1,
    '[' : 2,
    '{' : 3,
    '<' : 4
}


with open('./day10/input10.txt') as f:
    lines = f.readlines()
    partOneScore = 0
    partTwoScores = []
    for line in lines:
        stack = []
        partTwoScore = 0
        for char in line.strip():
            if char in OPENERS : stack.append(char)
            elif char in CLOSERS :
                matched = stack.pop()
                if not (
                    matched == '(' and char == ')' or
                    matched == '{' and char == '}' or
                    matched == '[' and char == ']' or
                    matched == '<' and char == '>' 
                ):
                    partOneScore += scoreLookUp[char]
                    stack = []
                    break
            else : print('invalid character')
        while len(stack) > 0 :
            currentChar = stack.pop()
            partTwoScore = partTwoScore * 5 + scoreLookUp[currentChar]
        if partTwoScore > 0 : partTwoScores.append(partTwoScore)
    finalScorePartTwo = math.floor(statistics.median(partTwoScores))
    print(f'Part One Answer: {partOneScore}')

    print(f'Part Two Answer: {finalScorePartTwo}')

#END OF PROGRAM